﻿using System.Collections.ObjectModel;
using System.IO;
using System.Windows;
using System.Text.RegularExpressions;
using TestDataConvert;
using System.Linq;
using System.Windows.Input;
using System.Windows.Controls;
using System;

namespace TestDataConvert
{
	/// <summary>
	/// MainWindow.xaml の相互作用ロジック
	/// </summary>
	public partial class MainWindow : Window
	{
		public MainWindow()
		{
			InitializeComponent();
		}
		private void Window_Drop(object sender, DragEventArgs e)
		{
			
			FileList list = this.DataContext as FileList;
			string[] files = e.Data.GetData(DataFormats.FileDrop) as string[];
			
			if (files != null)
			{
				Array.Sort<string>(files, delegate (string a, string b)
				{
					// ファイル名でソート
					return a.CompareTo(b);
				});

				int i=0;
				foreach (var s in files)
				{
					i++;
					var sTo = Directory.GetParent(s) + @"\TransactionLog_" + i.ToString().PadLeft(3,'0') + ".log";
					list.FileNames.Add(s + " To " + sTo);
					System.IO.File.Move(s, sTo);
				}

			}

		}
		private void listBoxItem_MouseDoubleClick(object sender, MouseButtonEventArgs e)
		{
			var listBoxItem = sender as ListBoxItem;
			this.LblGuide.Content = listBoxItem.DataContext;
			// ほげほげ
		}
	}
	public class FileList
	{
		public FileList()
		{
			FileNames = new ObservableCollection<string>();
		}
		public ObservableCollection<string> FileNames
		{
			get;
			private set;
		}
	}


}