﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Training
{
    public partial class FrmPrmControls : Form
    {
        private Label[] LblTitle;
        private Label[] LblValue;
        private Button[] BtnItem;


        public FrmPrmControls()
        {
            InitializeComponent();
        }

        private void FrmPrmControls_Load(object sender, EventArgs e)
        {
            //仮想コントロール配列の作成 foreach版

            //LblTitle = new Label[5];
            //LblValue = new Label[5];

            //foreach (var lbl in this.Controls.OfType<Label>())
            //{
            //    if (lbl.Name.Substring(0, 9) == "LblTitle_")
            //    {
            //        int i;
            //        i = Convert.ToInt32(lbl.Name.Substring(9, 1));
            //        LblTitle[i] = lbl;
            //    }
            //    else if (lbl.Name.Substring(0, 9) == "LblValue_")
            //    {
            //        int i;
            //        i = Convert.ToInt32(lbl.Name.Substring(9, 1));
            //        LblValue[i] = lbl;
            //    }
            //}

            //foreach (var btn in this.Controls.OfType<Button>())
            //{
            //    if (btn.Name.Substring(0, 4) == "Btn_")
            //    {
            //        int i;
            //        i = Convert.ToInt32(btn.Name.Substring(4, 1));
            //        BtnItem[i] = btn;
            //    }
            //}


            //仮想コントロール配列の作成 LINQ版

            //Formのコントロールからラベルを抜き出して名前順に並べ替え
            LblTitle = getControlPrmLbl("LblTitle_");
            LblValue = getControlPrmLbl("LblValue_");
            BtnItem = getControlPrmBtn("Btn_");

            //配列にセットされたことを確認
            for (int i = 0; i < LblTitle.Length; i++ )
            {
                LblTitle[i].Text = LblTitle[i].Name;
                LblValue[i].Text = LblValue[i].Name;
                BtnItem[i].Text = BtnItem[i].Name;
            }

        }

        private Label[] getControlPrmLbl(string sName)
        {
            var lbl= this.Controls.OfType<Label>()
                .Where(c => c.Name.Substring(0, sName.Length) == sName)
                .OrderBy(t => t.Name);
            return lbl.ToArray();           //並べ替えたものをコントロール配列として返す

        }
        private Button[] getControlPrmBtn(string sName)
        {
            var btn = this.Controls.OfType<Button>()
                .Where(c => c.Name.Substring(0, sName.Length) == sName)
                .OrderBy(t => t.Name);
            return btn.ToArray();           //並べ替えたものをのコントロール配列として返す

        }
    }
}
