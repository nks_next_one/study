﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Training
{

    //デリゲートでメソッドを宣言
    delegate string Training1(int i, string s);

    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
        }

        //デリゲートを使用するメソッドを宣言
        private static void show_Num(int i, string s, Training1 training)
        {

            MessageBox.Show(training(i, s));

        }

        private void button1_Click(object sender, EventArgs e)
        {

            show_Num(1, "a", (i, s) => i.ToString() + s);

        }

        //Actionの使用
        private void button2_Click(object sender, EventArgs e)
        {

            Action<string> funcA = (string s) =>
            {
                switch (s)
                {
                    case "a":
                        MessageBox.Show("1");
                        break;
                    case "b":
                        MessageBox.Show("2");
                        break;
                    default:
                        MessageBox.Show("3");
                        break;
                }

                return;
            };

            funcA("a");

            funcA("b");


        }

        //Funcの使用
        private void button3_Click(object sender, EventArgs e)
        {
            Func<string, int, string> funcA = (s, i) =>
            {
                string sStr = string.Empty;
                for (int cnt = 1; cnt <= i; cnt++)
                {
                    sStr += s;
                }
                return sStr;
            };


            MessageBox.Show(funcA("A", 5));
            MessageBox.Show(funcA("B", 3));
        }

        //LINQの使用
        private void button4_Click(object sender, EventArgs e)
        {
            int[] iPrm = { 1, 20, 4, 7, 2, 41, 10, 8, 16, 9, -5 };
            int iRange = 10;

            //LINQ の First を使用
            //最初に見つかった値を表示
            MessageBox.Show("最初に見つかった値：" + iPrm.First(i => i <= iRange).ToString());

            //一覧の中から条件に当てはまるものを抜き出す
            var iNuki = new List<int>();

            //LINQ の Where を使用
            foreach (int iOK in iPrm.Where(i => i <= iRange))
                iNuki.Add(iOK);            

            //抜き出したものを並べ替え
            iNuki.Sort();                   

            //並べ替えたものを順に表示
            string s = string.Empty;
            foreach (int iOK in iNuki)
                s += iOK.ToString() + "," ;

            MessageBox.Show("並べ替えた値：" + s);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            //int[] iPrm = { 1, 20, 4, 7, 2, 41, 10, 8, 16, 9, -5 };
            //int iRange = 10;

            ////LINQ の First を使用
            ////最初に見つかった値を表示
            //MessageBox.Show("最初に見つかった値：" + iPrm.First(i => i <= iRange).ToString());


            //var oNuki = iPrm.Where(i => i <= iRange)
            //                .OrderBy(t => t);
                            
            ////並べ替えたものを順に表示
            //string s = string.Empty;
            //foreach (int iOK in iNuki)
            //    s += iOK.ToString() + ",";

            //MessageBox.Show("並べ替えた値：" + s);
        }

        //LINQで仮想コントロール配列を作成
        private void button6_Click(object sender, EventArgs e)
        {
            using (FrmPrmControls frm = new FrmPrmControls())
            {
                frm.ShowDialog();
            }          
        }
    }
}
