﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestDataConvert
{
	public class patternSetting
	{
		public static List<patternSetting> SettingGroup = new List<patternSetting>();

        public enum patternTypes {Replace = 0 , Original = 1 };

        public patternTypes type { get; set; }
        public string name { get; set; }
		public string pattern { get; set; }
		public string replacement { get; set; }

		public patternSetting(patternTypes type, string name, string pattern, string replacement)
		{
            this.type = type;
			this.name = name;
			this.pattern = pattern;
			this.replacement = replacement;

			SettingGroup.Add(this);
		}

        public static string Unnecessary_FunctionMode = "(Initial|MainMenu)";

        /*
        public static patternSetting pattern_sample = new patternSetting(
            patternTypes.Replace
            , "（置換サンプル）"
            , @"(\d{4}[/\./]\d{2}[/\./]\d{2}\x20\d{2}[/\.:]\d{2}[/\.:]\d{2}[/\..]\d{3}\t\d+\t\d+\t\""(?i)Void\""\t\d+\t\d+\t\d+\t)((?i)22048.[^\n]*\n)(.[^\n]*\n)"
            , @"$1" + "\t1000001\t");
        */

        public static patternSetting pattern_Unnecessary_FunctionMode = new patternSetting(
            patternTypes.Replace
            , "不要なFunctionModeを削除"
            , @"(\d{4}[/\./]\d{2}[/\./]\d{2}\x20\d{2}[/\.:]\d{2}[/\.:]\d{2}[/\..]\d{3}\t\d+\t\d+\t\""(?i)"+ Unnecessary_FunctionMode + @"\""\t\d+\t\d+\t\d+\t.[^\n]*\n)"
            , "");

        public static patternSetting pattern_ExchngeItem_22048_Upd = new patternSetting(
            patternTypes.Replace
            , "お取替えレシート呼出し"
            , @"(\d{4}[/\./]\d{2}[/\./]\d{2}\x20\d{2}[/\.:]\d{2}[/\.:]\d{2}[/\..]\d{3}\t\d+\t\d+\t\""(?i)ExchangeItem\""\t\d+\t\d+\t\d+\t)(?i)22048.[^\n]*\n.*\d{4}[/\./]\d{2}[/\./]\d{2}\x20\d{2}[/\.:]\d{2}[/\.:]\d{2}[/\..]\d{3}\t\d+\t\d+\t\""(?i)ExchangeItem\""\t\d+\t\d+\t\d+\t(?i)2014.[^\n]*\n"
            , "${1}" + "\t1000001\tTrue");
        
		public static patternSetting pattern_Void_2014_Upd = new patternSetting(
            patternTypes.Replace
            , "返品VDレシート呼出し"
            , @"(\d{4}[/\./]\d{2}[/\./]\d{2}\x20\d{2}[/\.:]\d{2}[/\.:]\d{2}[/\..]\d{3}\t\d+\t\d+\t\""(?i)Void\""\t\d+\t\d+\t\d+\t(?i)22048.[^\n]*\n)(\d{4}[/\./]\d{2}[/\./]\d{2}\x20\d{2}[/\.:]\d{2}[/\.:]\d{2}[/\..]\d{3}\t\d+\t\d+\t\""(?i)Void\""\t\d+\t\d+\t\d+\t)((?i)2014.[^\n]*\n)"
            , "${1}" + "\t1000001\t");

        public static patternSetting pattern_Emoney_1000002_Add = new patternSetting(
            patternTypes.Replace
            , "電子マネー決済"
            , @"(\d{4}[/\./]\d{2}[/\./]\d{2}\x20\d{2}[/\.:]\d{2}[/\.:]\d{2}[/\..]\d{3}\t\d+\t\d+\t\"".[^\""]*\""\t\d+\t\d+\t\d+\t(?i)11006.[^\n]*\n)"
            , "${1}" + "\n2099/01/17 00:00:00.000\t0\t0\t\"" + "\"\t111\t0\t0\t1000002\t\"0, 7, 8600230000000007\"\n");

        public static patternSetting pattern_Emoney_500013_Del = new patternSetting(
            patternTypes.Replace
            , "電子マネー決済"
            , @"\d{4}[/\./]\d{2}[/\./]\d{2}\x20\d{2}[/\.:]\d{2}[/\.:]\d{2}[/\..]\d{3}\t\d+\t\d+\t\"".[^\""]*\""\t\d+\t\d+\t\d+\t(?i)500013.[^\n]*"
            , "");

        public static patternSetting pattern_Emoney_22031_Upd = new patternSetting(
            patternTypes.Replace
            , "電子マネー決済(残高不足、現金併用支払）"
            , @"(\d{4}[/\./]\d{2}[/\./]\d{2}\x20\d{2}[/\.:]\d{2}[/\.:]\d{2}[/\..]\d{3}\t\d+\t\d+\t\"".[^\""]*\""\t\d+\t\d+\t\d+\t(?i)22031\t\"")(\"")((?i)EMoneyInputEMoneyBalance.[^\n]*)(\"")(\n)"
            , "$1$3$5");

        public static patternSetting pattern_Point_1000004_Add = new patternSetting(
            patternTypes.Replace
            , "ポイントサービス"
            , @"(\d{4}[/\./]\d{2}[/\./]\d{2}\x20\d{2}[/\.:]\d{2}[/\.:]\d{2}[/\..]\d{3}\t\d+\t\d+\t\"".[^\""]*\""\t\d+\t\d+\t\d+\t(?i)11006.[^\n]*\n)"
            , "2099/01/17 00:00:00.000\t0\t0\t\"" + "\"\t117\t0\t0\t1000004\t\"False, 498, 20150102\"\n$1");

        public static patternSetting pattern_Point_1000005_Upd = new patternSetting(
            patternTypes.Replace
            , "ポイントサービス"
            , @"(\d{4}[/\./]\d{2}[/\./]\d{2}\x20\d{2}[/\.:]\d{2}[/\.:]\d{2}[/\..]\d{3}\t\d+\t\d+\t\"".[^\""]*\""\t\d+\t\d+\t\d+\t)((?i)500001)(.[^\n]*\n)"
            , "${1}" + "1000005$3");
    }
}
