﻿using System.Collections.ObjectModel;
using System.IO;
using System.Windows;
using System.Text.RegularExpressions;
using TestDataConvert;
using System.Linq;
using System.Windows.Input;
using System.Windows.Controls;

namespace TestDataConvert
{
	/// <summary>
	/// MainWindow.xaml の相互作用ロジック
	/// </summary>
	public partial class MainWindow : Window
	{
		public MainWindow()
		{
			InitializeComponent();
		}
		private void Window_Drop(object sender, DragEventArgs e)
		{
			
			FileList list = this.DataContext as FileList;
			string[] files = e.Data.GetData(DataFormats.FileDrop) as string[];
			if (files != null)
            {
                var sOutputFile = Directory.GetParent(files[0]) + @"\PlayList.log";
                if (File.Exists(sOutputFile) == true) File.Delete(sOutputFile);

                foreach (var s in files)
				{
					var sSource = File.ReadAllText(s,System.Text.Encoding.GetEncoding("shift_jis"));
					
					
					//patternSetting.SettingGroup.ForEach(x => sSource = Regex.Replace(sSource, x.pattern, x.replacement));                    					
					foreach (var x in patternSetting.SettingGroup)
					{
						sSource = Regex.Replace(sSource, x.pattern, x.replacement);
					}	

					// ファイルfile.txtを作成して開く
					using (FileStream stream = File.Create(sOutputFile))
					{
						list.FileNames.Add(s);
						// FileStreamに書き込むためのStreamWriterを作成
						using (StreamWriter writer = new StreamWriter(stream))
						{
							// 文字列を改行付きで書き込む
							writer.Write(sSource);

						}
					}
				}

			}
		}		private void listBoxItem_MouseDoubleClick(object sender, MouseButtonEventArgs e)
		{
			var listBoxItem = sender as ListBoxItem;
			this.LblGuide.Content = listBoxItem.DataContext;
			// ほげほげ
		}
	}
	public class FileList
	{
		public FileList()
		{
			FileNames = new ObservableCollection<string>();
		}
		public ObservableCollection<string> FileNames
		{
			get;
			private set;
		}
	}


}