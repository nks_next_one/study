﻿namespace Training
{
    partial class FrmPrmControls
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LblTitle_0 = new System.Windows.Forms.Label();
            this.LblTitle_1 = new System.Windows.Forms.Label();
            this.LblTitle_2 = new System.Windows.Forms.Label();
            this.LblTitle_3 = new System.Windows.Forms.Label();
            this.LblTitle_4 = new System.Windows.Forms.Label();
            this.LblValue_4 = new System.Windows.Forms.Label();
            this.LblValue_3 = new System.Windows.Forms.Label();
            this.LblValue_2 = new System.Windows.Forms.Label();
            this.LblValue_1 = new System.Windows.Forms.Label();
            this.LblValue_0 = new System.Windows.Forms.Label();
            this.Btn_0 = new System.Windows.Forms.Button();
            this.Btn_1 = new System.Windows.Forms.Button();
            this.Btn_2 = new System.Windows.Forms.Button();
            this.Btn_3 = new System.Windows.Forms.Button();
            this.Btn_4 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // LblTitle_0
            // 
            this.LblTitle_0.BackColor = System.Drawing.Color.Khaki;
            this.LblTitle_0.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.LblTitle_0.Font = new System.Drawing.Font("MS UI Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.LblTitle_0.Location = new System.Drawing.Point(28, 23);
            this.LblTitle_0.Name = "LblTitle_0";
            this.LblTitle_0.Size = new System.Drawing.Size(89, 22);
            this.LblTitle_0.TabIndex = 0;
            this.LblTitle_0.Text = "label1";
            // 
            // LblTitle_1
            // 
            this.LblTitle_1.BackColor = System.Drawing.Color.Khaki;
            this.LblTitle_1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.LblTitle_1.Font = new System.Drawing.Font("MS UI Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.LblTitle_1.Location = new System.Drawing.Point(28, 45);
            this.LblTitle_1.Name = "LblTitle_1";
            this.LblTitle_1.Size = new System.Drawing.Size(89, 22);
            this.LblTitle_1.TabIndex = 1;
            this.LblTitle_1.Text = "label1";
            // 
            // LblTitle_2
            // 
            this.LblTitle_2.BackColor = System.Drawing.Color.Khaki;
            this.LblTitle_2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.LblTitle_2.Font = new System.Drawing.Font("MS UI Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.LblTitle_2.Location = new System.Drawing.Point(28, 67);
            this.LblTitle_2.Name = "LblTitle_2";
            this.LblTitle_2.Size = new System.Drawing.Size(89, 22);
            this.LblTitle_2.TabIndex = 2;
            this.LblTitle_2.Text = "label1";
            // 
            // LblTitle_3
            // 
            this.LblTitle_3.BackColor = System.Drawing.Color.Khaki;
            this.LblTitle_3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.LblTitle_3.Font = new System.Drawing.Font("MS UI Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.LblTitle_3.Location = new System.Drawing.Point(28, 89);
            this.LblTitle_3.Name = "LblTitle_3";
            this.LblTitle_3.Size = new System.Drawing.Size(89, 22);
            this.LblTitle_3.TabIndex = 3;
            this.LblTitle_3.Text = "label1";
            // 
            // LblTitle_4
            // 
            this.LblTitle_4.BackColor = System.Drawing.Color.Khaki;
            this.LblTitle_4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.LblTitle_4.Font = new System.Drawing.Font("MS UI Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.LblTitle_4.Location = new System.Drawing.Point(28, 111);
            this.LblTitle_4.Name = "LblTitle_4";
            this.LblTitle_4.Size = new System.Drawing.Size(89, 22);
            this.LblTitle_4.TabIndex = 4;
            this.LblTitle_4.Text = "label1";
            // 
            // LblValue_4
            // 
            this.LblValue_4.BackColor = System.Drawing.Color.White;
            this.LblValue_4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.LblValue_4.Font = new System.Drawing.Font("MS UI Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.LblValue_4.Location = new System.Drawing.Point(116, 111);
            this.LblValue_4.Name = "LblValue_4";
            this.LblValue_4.Size = new System.Drawing.Size(125, 22);
            this.LblValue_4.TabIndex = 9;
            // 
            // LblValue_3
            // 
            this.LblValue_3.BackColor = System.Drawing.Color.White;
            this.LblValue_3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.LblValue_3.Font = new System.Drawing.Font("MS UI Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.LblValue_3.Location = new System.Drawing.Point(116, 89);
            this.LblValue_3.Name = "LblValue_3";
            this.LblValue_3.Size = new System.Drawing.Size(125, 22);
            this.LblValue_3.TabIndex = 8;
            // 
            // LblValue_2
            // 
            this.LblValue_2.BackColor = System.Drawing.Color.White;
            this.LblValue_2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.LblValue_2.Font = new System.Drawing.Font("MS UI Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.LblValue_2.Location = new System.Drawing.Point(116, 67);
            this.LblValue_2.Name = "LblValue_2";
            this.LblValue_2.Size = new System.Drawing.Size(125, 22);
            this.LblValue_2.TabIndex = 7;
            // 
            // LblValue_1
            // 
            this.LblValue_1.BackColor = System.Drawing.Color.White;
            this.LblValue_1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.LblValue_1.Font = new System.Drawing.Font("MS UI Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.LblValue_1.Location = new System.Drawing.Point(116, 45);
            this.LblValue_1.Name = "LblValue_1";
            this.LblValue_1.Size = new System.Drawing.Size(125, 22);
            this.LblValue_1.TabIndex = 6;
            // 
            // LblValue_0
            // 
            this.LblValue_0.BackColor = System.Drawing.Color.White;
            this.LblValue_0.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.LblValue_0.Font = new System.Drawing.Font("MS UI Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.LblValue_0.Location = new System.Drawing.Point(116, 23);
            this.LblValue_0.Name = "LblValue_0";
            this.LblValue_0.Size = new System.Drawing.Size(125, 22);
            this.LblValue_0.TabIndex = 5;
            // 
            // Btn_0
            // 
            this.Btn_0.Location = new System.Drawing.Point(247, 22);
            this.Btn_0.Name = "Btn_0";
            this.Btn_0.Size = new System.Drawing.Size(75, 26);
            this.Btn_0.TabIndex = 10;
            this.Btn_0.Text = "button1";
            this.Btn_0.UseVisualStyleBackColor = true;
            // 
            // Btn_1
            // 
            this.Btn_1.Location = new System.Drawing.Point(247, 44);
            this.Btn_1.Name = "Btn_1";
            this.Btn_1.Size = new System.Drawing.Size(75, 26);
            this.Btn_1.TabIndex = 11;
            this.Btn_1.Text = "button2";
            this.Btn_1.UseVisualStyleBackColor = true;
            // 
            // Btn_2
            // 
            this.Btn_2.Location = new System.Drawing.Point(247, 66);
            this.Btn_2.Name = "Btn_2";
            this.Btn_2.Size = new System.Drawing.Size(75, 26);
            this.Btn_2.TabIndex = 12;
            this.Btn_2.Text = "button3";
            this.Btn_2.UseVisualStyleBackColor = true;
            // 
            // Btn_3
            // 
            this.Btn_3.Location = new System.Drawing.Point(247, 88);
            this.Btn_3.Name = "Btn_3";
            this.Btn_3.Size = new System.Drawing.Size(75, 26);
            this.Btn_3.TabIndex = 13;
            this.Btn_3.Text = "button4";
            this.Btn_3.UseVisualStyleBackColor = true;
            // 
            // Btn_4
            // 
            this.Btn_4.Location = new System.Drawing.Point(247, 110);
            this.Btn_4.Name = "Btn_4";
            this.Btn_4.Size = new System.Drawing.Size(75, 26);
            this.Btn_4.TabIndex = 14;
            this.Btn_4.Text = "button5";
            this.Btn_4.UseVisualStyleBackColor = true;
            // 
            // FrmPrmControls
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(408, 296);
            this.Controls.Add(this.Btn_4);
            this.Controls.Add(this.Btn_3);
            this.Controls.Add(this.Btn_2);
            this.Controls.Add(this.Btn_1);
            this.Controls.Add(this.Btn_0);
            this.Controls.Add(this.LblValue_4);
            this.Controls.Add(this.LblValue_3);
            this.Controls.Add(this.LblValue_2);
            this.Controls.Add(this.LblValue_1);
            this.Controls.Add(this.LblValue_0);
            this.Controls.Add(this.LblTitle_4);
            this.Controls.Add(this.LblTitle_3);
            this.Controls.Add(this.LblTitle_2);
            this.Controls.Add(this.LblTitle_1);
            this.Controls.Add(this.LblTitle_0);
            this.Name = "FrmPrmControls";
            this.Text = "FrmPrmControls";
            this.Load += new System.EventHandler(this.FrmPrmControls_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label LblTitle_0;
        private System.Windows.Forms.Label LblTitle_1;
        private System.Windows.Forms.Label LblTitle_2;
        private System.Windows.Forms.Label LblTitle_3;
        private System.Windows.Forms.Label LblTitle_4;
        private System.Windows.Forms.Label LblValue_4;
        private System.Windows.Forms.Label LblValue_3;
        private System.Windows.Forms.Label LblValue_2;
        private System.Windows.Forms.Label LblValue_1;
        private System.Windows.Forms.Label LblValue_0;
        private System.Windows.Forms.Button Btn_0;
        private System.Windows.Forms.Button Btn_1;
        private System.Windows.Forms.Button Btn_2;
        private System.Windows.Forms.Button Btn_3;
        private System.Windows.Forms.Button Btn_4;
    }
}